# Devops-spe

## Sommaire

- Objectif du projet
- Description des stages de la CI
- Liens pour voir les différents points du projet

## Objectif du projet
Ce projet présente une CI complète passant du test du code, à la compilation au test du binaire juqu'au déploiement d'une image docker avec l'application.

D'abord il y a le code source, qui est situé dans trois fichiers :
 - [`/main.go`](/main.go)
 - [`/server.go`](/server.go)
 - [`/server_test.go`](/server_test.go)
Il s'agit d'un serveur web qui répond aux requêtes GET à la racine et sur le port 5000 avec un message statique : `Hello DevOps`. Le fichier de test unitaire [`server_test.go`](/server_test.go) permet de vérifier que lors d'une requête la réponse retournée est bien `Hello DevOps`

La pipeline quant à elle lance les tests unitaires puis compile l'application pour la placer dans une image Docker testée et sécurisée, et ce à chaque commit dans la branche `master`

## Description des stages de la CI
### stage : test
Il s'agit du premier stage de la pipeline qui lance les tests unitaires en go. Si les tests echouent, tout s'arrête là.

### stage : compile
Lance la compilation du projet GO et crée un binaire nommé `go-server`. Ce binaire est gardé comme artifact pour les prochains stage de la pipeline.

### stage : build
Il utilise le service de Docker-in-Docker pour créer une image docker contenant le binaire `go-server` obtenu à partir des artifacts, et place la nouvelle image dans le registry avec le nom et tag suivant : `registry.gitlab.com/thottou1/devops-spe:$CI_COMMIT_SHORT_SHA`.

**Note:** le [`Dockerfile`](/Dockerfile) est utilisé pour le build. Il copy le binaire `./go-server` et le place dans `/server`, expose le port `5000` et son entrypoint est `/server`. L'entrypoint permet de faire un `docker run` et c'est comme si nous lancions le binaire go directement. Le `RUN ls -l /` est un test bidon pour les permissions, donc à ignorer.

### stage : docker-test
L'étape docker-test lance la nouvelle image docker et la fait tourner en arrière-plan, pour ensuite utiliser une image docker avec l'utilitaire `curl` pour y lancer une requête GET à la racine et au port `5000`. l'image pour curl est faite de telle sorte que si la requête échoue, l'étape échoue et la pipeline est stoppée.

### stage : container-scanning
L'étape container-scanning est l'étape de la pipeline la plus complexe. Nous utilisons clair et clair-scanner plus précisement pour détecter des vulnérabilités dans l'image docker contenant notre application. Pour cela nous lancer un container docker pour la base de données de clair, puis nous utilisons une autre image qui lance `clair-scanner` et qui utilise la base de données clair pour scanner notre image à nous. 

**Note:** Gitlab semble utiliser une version du kernel Linux qui a des failles, et donc lors du scan de notre image il y avait des failles qui n'avaient pas de rapport avec notre projet à nous. Pour aller plus loin dans le projet j'ai choisi de les ajouter à la whitelist (voir fichier [`cve-whitelist.yml`](/cve-whitelist.yml)) des CVE. Ces failles de sécurités sont donc ignorées. Et la pipeline continue.

### stage : release
Le stage release, qui est le dernier stage de la pipeline récupère l'image docker que nous avons crées et scannés plus tôt dans la pipeline et lui ajoute un tag `release-` pour préciser qu'elle est valide et prête à être utilisée. Ainsi lorsque nous allons dans le registry nous pouvons voir les images docker prêtes pour la production.

## Liens pour voir les différents points du projet

- Pour voir les pipelines, utiliser le lien suivant : https://gitlab.com/thottou1/devops-spe/-/pipelines
- Pour voir les containers, utiliser le lien suivant : https://gitlab.com/thottou1/devops-spe/container_registry
- Pour voir les fichiers, utiliser le lien suivant : https://gitlab.com/thottou1/devops-spe/-/tree/master
- Pour voir les commits, utiliser le lien suivant : https://gitlab.com/thottou1/devops-spe/-/commits/master (note : certains commits ont des noms basiques car ils ne servaient qu'à trigger la pipeline. Et je remarque aussi que j'ai commit en utilisant mon compte github Aelto 🤷‍♂️)