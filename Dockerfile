FROM golang:1.14.4

COPY ./go-server /server
RUN ls -l /
EXPOSE 5000
ENTRYPOINT [ "/server" ]